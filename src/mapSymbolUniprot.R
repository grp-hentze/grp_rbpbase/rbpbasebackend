
mapSymbolUniprot <- function(x, 
                             studyname,
                             organism = "Hs", 
                             value = 2, 
                             case_sensitive = TRUE,
                             manual_mapping = NULL,
                             LOOKUP = NULL) {
    
    stopifnot(LOOKUP != NULL)
    
    # Creating a mapping table
    MAPPING <- bind_rows(
        LOOKUP[[paste0(organism, "_EnsemblID_UniProtSwissProt")]] %>% 
            filter(Foreign != "") %>% 
            mutate(IDtype = "protein_id"),
        
        LOOKUP[[paste0(organism, "_EnsemblID_Name_Description")]] %>% 
            dplyr::select(ID, Name) %>%
            dplyr::rename(Foreign = Name)  %>% 
            mutate(IDtype = "gene_name")
    )
    
    if(!is.null(manual_mapping)) {
        MAPPING <- bind_rows(MAPPING,
                  manual_mapping)
    }
    
    #prepare initial table
    x <- x %>% mutate(RID = seq.int(n())) %>% 
        separate_rows(protein_id, sep = "\\|") %>%
        separate_rows(gene_name,  sep = "\\|") %>% 
        mutate(KID = seq.int(n())) %>%
        gather(key = "IDtype", 
               value = "IDforeign",
               c("gene_name", "protein_id") )
    
    if(!case_sensitive) {
        MAPPING <- MAPPING %>% mutate(Foreign = toupper(Foreign))
        x <- x %>% mutate(IDforeign = toupper(IDforeign))
    }
    
    
    x %>% left_join(y = MAPPING, 
                  by = c("IDforeign" = "Foreign",
                         "IDtype"    = "IDtype")) %>%
        group_by(RID) %>% 
        mutate(foundID = !is.na(ID)) %>% 
        arrange(RID, desc(foundID), KID, IDtype) %>%
        dplyr::slice(1) %>% ungroup() %>%
        dplyr::select(IDforeign, ID) %>%
        dplyr::rename(UNIQUE = IDforeign) %>%
        mutate(RBPBASEID = studyname, organism = organism, value = value) %>%  
        dplyr::select(-UNIQUE, -organism) %>% dplyr::rename(GENEID = ID)
}

