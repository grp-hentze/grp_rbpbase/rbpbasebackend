# RBPbase Backend

This repository prepares compiles annotation,
high-throughput RNA-binding protein (RBP) detection studies,
and homology mapping to preprocessed tables which 
then will be used either for analysis
(RBPbaseAnalysis and any RBPbaseAnalysis* repositories) or
for display via the shiny server RBPbase (see RBPbase repository).

## Data input

RBP studies, annotations and homology mapping 


## Compilation

Executing this script will build tables for 
 internal and external use

```{r}
Rscript make.R
```