---
title: "04_Stats.Rmd"
author: "Thomas Schwarzl"
output: BiocStyle::html_document
---



```{r}
require(tidyverse)
require(ggExtra)
 #require(ggthemes)
```


```{r}
load("out/RIC_RBPS.Rda")
load("out/RIC_STUDIES.Rda")
load("out/RBPANNO.Rda")
load("out/ANNO_STUDIES.Rda")
```




```{r}
x <- RIC_RBPS %>% left_join(y = RIC_STUDIES, by = c("RBPBASEID")) %>% dplyr::filter(value > 1)

x %>% group_by(Organism, RBPBASEID) %>% summarise(count = n())

```



```{r}
x %>% dplyr::select(RBPBASEID, GENEID, Organism) %>%
      dplyr::distinct() %>% 
      group_by(Organism) %>%
      summarise(count = n())
```

```{r}
y <- x %>% dplyr::select(RBPBASEID, Organism) %>%
      dplyr::distinct() %>% 
      group_by(Organism) %>%
      summarise(count = n()) %>% 
     mutate(Organism = factor(Organism, levels = Organism[rev(order(count))]))
```


```{r}
y %>% 
    ggplot(aes(x = Organism,
               fill = Organism,
               y = count)) + 
    geom_bar(stat = "identity") + 
    geom_text(aes(label = count),
              size      = 5, 
              vjust     = -0.2) + 
    theme_minimal() + scale_fill_brewer(palette = "Dark2") +
    theme(legend.position = "none") + removeGrid() +
    theme(axis.title.y = element_blank(),
          axis.text.y  = element_blank(),
          axis.ticks.y = element_blank(),
        axis.text.x = element_text(size = 17)) + xlab("")

```
## Annotation



```{r}

ax <- RBPANNO %>% left_join(y = ANNO_STUDIES, by = c("RBPBASEID"))
ax %>% group_by(Organism, RBPBASEID) %>% summarise(count = n())


ay <- ax %>% dplyr::select(RBPBASEID, Organism) %>%
      dplyr::distinct() %>% 
      group_by(Organism) %>%
      summarise(count = n()) %>% 
     mutate(Organism = factor(Organism, levels = Organism[rev(order(count))]))
```


```{r}
ay %>% 
    ggplot(aes(x = Organism,
               fill = Organism,
               y = count)) + 
    geom_bar(stat = "identity") + 
    geom_text(aes(label = count),
              size      = 5, 
              vjust     = -0.2) + 
    theme_minimal() + scale_fill_brewer(palette = "Dark2") +
    theme(legend.position = "none") + removeGrid() +
    theme(axis.title.y = element_blank(),
          axis.text.y  = element_blank(),
          axis.ticks.y = element_blank(),
        axis.text.x = element_text(size = 17)) + xlab("")

```



