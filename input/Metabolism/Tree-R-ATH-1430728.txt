R-ATH-1430728@Metabolism
R-ATH-1362409@@Mitochondrial iron-sulfur cluster biogenesis
R-ATH-2395516@@@Electron transport from NADPH to Ferredoxin
R-ATH-1428517@@The citric acid (TCA) cycle and respiratory electron transport
R-ATH-163200@@@Respiratory electron transport, ATP synthesis by chemiosmotic coupling, and heat production by uncoupling proteins.
R-ATH-166187@@@@Mitochondrial Uncoupling Proteins
R-ATH-167826@@@@@The fatty acid cycling model
R-ATH-167827@@@@@The proton buffering model
R-ATH-611105@@@@Respiratory electron transport
R-ATH-6799198@@@@@Complex I biogenesis
R-ATH-71406@@@Pyruvate metabolism and Citric Acid (TCA) cycle
R-ATH-70268@@@@Pyruvate metabolism
R-ATH-204174@@@@@Regulation of pyruvate dehydrogenase (PDH) complex
R-ATH-71403@@@@Citric acid cycle (TCA cycle)
R-ATH-880009@@@@Interconversion of 2-oxoglutarate and 2-hydroxyglutarate
R-ATH-1475029@@Reversible hydration of carbon dioxide
R-ATH-1480926@@O2/CO2 exchange in erythrocytes
R-ATH-1237044@@@Erythrocytes take up carbon dioxide and release oxygen
R-ATH-1247673@@@Erythrocytes take up oxygen and release carbon dioxide
R-ATH-1483249@@Inositol phosphate metabolism
R-ATH-1855167@@@Synthesis of pyrophosphates in the cytosol
R-ATH-1855183@@@Synthesis of IP2, IP, and Ins in the cytosol
R-ATH-1855191@@@Synthesis of IPs in the nucleus
R-ATH-1855204@@@Synthesis of IP3 and IP4 in the cytosol
R-ATH-1855231@@@Synthesis of IPs in the ER lumen
R-ATH-15869@@Metabolism of nucleotides
R-ATH-2393930@@@Phosphate bond hydrolysis by NUDT proteins
R-ATH-499943@@@Synthesis and interconversion of nucleotide di- and triphosphates
R-ATH-73847@@@Purine metabolism
R-ATH-73817@@@@Purine ribonucleoside monophosphate biosynthesis
R-ATH-74217@@@@Purine salvage
R-ATH-74259@@@@Purine catabolism
R-ATH-73848@@@Pyrimidine metabolism
R-ATH-500753@@@@Pyrimidine biosynthesis
R-ATH-73614@@@@Pyrimidine salvage reactions
R-ATH-73621@@@@Pyrimidine catabolism
R-ATH-8850843@@@Phosphate bond hydrolysis by NTPDase proteins
R-ATH-163685@@Integration of energy metabolism
R-ATH-163680@@@AMPK inhibits chREBP transcriptional activation activity
R-ATH-163754@@@Insulin effects increased synthesis of Xylulose-5-Phosphate
R-ATH-163765@@@ChREBP activates metabolic gene expression
R-ATH-422356@@@Regulation of insulin secretion
R-ATH-399997@@@@Acetylcholine regulates insulin secretion
R-ATH-400451@@@@Free fatty acids regulate insulin secretion
R-ATH-434316@@@@@Fatty Acids bound to GPR40 (FFAR1) regulate insulin secretion
R-ATH-189445@@Metabolism of porphyrins
R-ATH-189451@@@Heme biosynthesis
R-ATH-196854@@Metabolism of vitamins and cofactors
R-ATH-196849@@@Metabolism of water-soluble vitamins and cofactors
R-ATH-196741@@@@Cobalamin (Cbl, vitamin B12) transport and metabolism
R-ATH-196757@@@@Metabolism of folate and pterines
R-ATH-196780@@@@Biotin transport and metabolism
R-ATH-196807@@@@Nicotinate metabolism
R-ATH-197264@@@@@Nicotinamide salvaging
R-ATH-196819@@@@Vitamin B1 (thiamin) metabolism
R-ATH-196836@@@@Vitamin C (ascorbate) metabolism
R-ATH-196843@@@@Vitamin B2 (riboflavin) metabolism
R-ATH-199220@@@@Vitamin B5 (pantothenate) metabolism
R-ATH-196783@@@@@Coenzyme A biosynthesis
R-ATH-947581@@@@Molybdenum cofactor biosynthesis
R-ATH-964975@@@@Vitamins B6 activation to pyridoxal phosphate
R-ATH-6806667@@@Metabolism of fat-soluble vitamins
R-ATH-975634@@@@Retinoid metabolism and transport
R-ATH-202131@@Metabolism of nitric oxide
R-ATH-203765@@@eNOS activation and regulation
R-ATH-1474151@@@@Tetrahydrobiopterin (BH4) synthesis, recycling, salvage and regulation
R-ATH-211859@@Biological oxidations
R-ATH-156580@@@Phase II conjugation
R-ATH-156581@@@@Methylation
R-ATH-156584@@@@Cytosolic sulfonation of small molecules
R-ATH-174362@@@@@Transport and synthesis of PAPS
R-ATH-156588@@@@Glucuronidation
R-ATH-173599@@@@@Formation of the active cofactor, UDP-glucuronate
R-ATH-156590@@@@Glutathione conjugation
R-ATH-174403@@@@@Glutathione synthesis and recycling
R-ATH-211945@@@Phase 1 - Functionalization of compounds
R-ATH-140179@@@@Amine Oxidase reactions
R-ATH-141334@@@@@PAOs oxidise polyamines to amines
R-ATH-211897@@@@Cytochrome P450 - arranged by substrate type
R-ATH-211916@@@@@Vitamins
R-ATH-211935@@@@@Fatty acids
R-ATH-211958@@@@@Miscellaneous substrates
R-ATH-211976@@@@@Endogenous sterols
R-ATH-211979@@@@@Eicosanoids
R-ATH-211981@@@@@Xenobiotics
R-ATH-217271@@@@FMO oxidises nucleophiles
R-ATH-71384@@@@Ethanol oxidation
R-ATH-5423646@@@Aflatoxin activation and detoxification
R-ATH-2161522@@Abacavir transport and metabolism
R-ATH-2161541@@@Abacavir metabolism
R-ATH-556833@@Metabolism of lipids and lipoproteins
R-ATH-1483257@@@Phospholipid metabolism
R-ATH-1483206@@@@Glycerophospholipid biosynthesis
R-ATH-1482788@@@@@Acyl chain remodelling of PC
R-ATH-1482798@@@@@Acyl chain remodeling of CL
R-ATH-1482801@@@@@Acyl chain remodelling of PS
R-ATH-1482839@@@@@Acyl chain remodelling of PE
R-ATH-1482883@@@@@Acyl chain remodeling of DAG and TAG
R-ATH-1482922@@@@@Acyl chain remodelling of PI
R-ATH-1482925@@@@@Acyl chain remodelling of PG
R-ATH-1483076@@@@@Synthesis of CL
R-ATH-1483101@@@@@Synthesis of PS
R-ATH-1483148@@@@@Synthesis of PG
R-ATH-1483166@@@@@Synthesis of PA
R-ATH-1483191@@@@@Synthesis of PC
R-ATH-1483213@@@@@Synthesis of PE
R-ATH-1483226@@@@@Synthesis of PI
R-ATH-6798163@@@@@Choline catabolism
R-ATH-1483255@@@@PI Metabolism
R-ATH-1483248@@@@@Synthesis of PIPs at the ER membrane
R-ATH-1660499@@@@@Synthesis of PIPs at the plasma membrane
R-ATH-1660514@@@@@Synthesis of PIPs at the Golgi membrane
R-ATH-1660516@@@@@Synthesis of PIPs at the early endosome membrane
R-ATH-1660517@@@@@Synthesis of PIPs at the late endosome membrane
R-ATH-6814848@@@@@Glycerophospholipid catabolism
R-ATH-1655829@@@Regulation of cholesterol biosynthesis by SREBP (SREBF)
R-ATH-2426168@@@@Activation of gene expression by SREBF (SREBP)
R-ATH-191273@@@Cholesterol biosynthesis
R-ATH-6807047@@@@Cholesterol biosynthesis via desmosterol
R-ATH-6807062@@@@Cholesterol biosynthesis via lathosterol
R-ATH-194068@@@Bile acid and bile salt metabolism
R-ATH-159418@@@@Recycling of bile acids and salts
R-ATH-192105@@@@Synthesis of bile acids and bile salts
R-ATH-193368@@@@@Synthesis of bile acids and bile salts via 7alpha-hydroxycholesterol
R-ATH-193775@@@@@Synthesis of bile acids and bile salts via 24-hydroxycholesterol
R-ATH-193807@@@@@Synthesis of bile acids and bile salts via 27-hydroxycholesterol
R-ATH-196071@@@Metabolism of steroid hormones
R-ATH-193048@@@@Androgen biosynthesis
R-ATH-193144@@@@Estrogen biosynthesis
R-ATH-194002@@@@Glucocorticoid biosynthesis
R-ATH-196108@@@@Pregnenolone biosynthesis
R-ATH-2046104@@@alpha-linolenic (omega3) and linoleic (omega6) acid metabolism
R-ATH-2046105@@@@Linoleic acid (LA) metabolism
R-ATH-2046106@@@@alpha-linolenic acid (ALA) metabolism
R-ATH-2142753@@@Arachidonic acid metabolism
R-ATH-2142670@@@@Synthesis of epoxy (EET) and dihydroxyeicosatrienoic acids (DHET)
R-ATH-2142691@@@@Synthesis of Leukotrienes (LT) and Eoxins (EX)
R-ATH-2142696@@@@Synthesis of Hepoxilins (HX) and Trioxilins (TrX)
R-ATH-2142700@@@@Synthesis of Lipoxins (LX)
R-ATH-2142712@@@@Synthesis of 12-eicosatetraenoic acid derivatives
R-ATH-2142770@@@@Synthesis of 15-eicosatetraenoic acid derivatives
R-ATH-2142816@@@@Synthesis of (16-20)-hydroxyeicosatetraenoic acids (HETE)
R-ATH-2162123@@@@Synthesis of Prostaglandins (PG) and Thromboxanes (TX)
R-ATH-2142789@@@Ubiquinol biosynthesis
R-ATH-390918@@@Peroxisomal lipid metabolism
R-ATH-389599@@@@Alpha-oxidation of phytanate
R-ATH-389887@@@@Beta-oxidation of pristanoyl-CoA
R-ATH-390247@@@@Beta-oxidation of very long chain fatty acids
R-ATH-428157@@@Sphingolipid metabolism
R-ATH-1660661@@@@Sphingolipid de novo biosynthesis
R-ATH-1660662@@@@Glycosphingolipid metabolism
R-ATH-535734@@@Fatty acid, triacylglycerol, and ketone body metabolism
R-ATH-200425@@@@Import of palmitoyl-CoA into the mitochondrial matrix
R-ATH-74182@@@@Ketone body metabolism
R-ATH-77108@@@@@Utilization of Ketone Bodies
R-ATH-77111@@@@@Synthesis of Ketone Bodies
R-ATH-75109@@@@Triglyceride Biosynthesis
R-ATH-75105@@@@@Fatty Acyl-CoA Biosynthesis
R-ATH-75876@@@@@@Synthesis of very long-chain fatty acyl-CoAs
R-ATH-77289@@@@Mitochondrial Fatty Acid Beta-Oxidation
R-ATH-77286@@@@@mitochondrial fatty acid beta-oxidation of saturated fatty acids
R-ATH-77310@@@@@@Beta oxidation of lauroyl-CoA to decanoyl-CoA-CoA
R-ATH-77346@@@@@@Beta oxidation of decanoyl-CoA to octanoyl-CoA-CoA
R-ATH-77348@@@@@@Beta oxidation of octanoyl-CoA to hexanoyl-CoA
R-ATH-77350@@@@@@Beta oxidation of hexanoyl-CoA to butanoyl-CoA
R-ATH-77352@@@@@@Beta oxidation of butanoyl-CoA to acetyl-CoA
R-ATH-73923@@@Lipid digestion, mobilization, and transport
R-ATH-174824@@@@Lipoprotein metabolism
R-ATH-171052@@@@@LDL-mediated lipid transport
R-ATH-194223@@@@@HDL-mediated lipid transport
R-ATH-192456@@@@Digestion of dietary lipid
R-ATH-265473@@@@Trafficking of dietary sterols
R-ATH-8848584@@@Wax biosynthesis
R-ATH-71291@@Metabolism of amino acids and derivatives
R-ATH-1614635@@@Sulfur amino acid metabolism
R-ATH-1237112@@@@Methionine salvage pathway
R-ATH-1614558@@@@Degradation of cysteine and homocysteine
R-ATH-1614517@@@@@Sulfide oxidation to sulfate
R-ATH-1614603@@@@Cysteine formation from homocysteine
R-ATH-209776@@@Amine-derived hormones
R-ATH-209905@@@@Catecholamine biosynthesis
R-ATH-209931@@@@Serotonin and melatonin biosynthesis
R-ATH-209968@@@@Thyroxine biosynthesis
R-ATH-2408522@@@Selenoamino acid metabolism
R-ATH-2408508@@@@Metabolism of ingested SeMet, Sec, MeSec into H2Se
R-ATH-5263617@@@@Metabolism of ingested MeSeO2H into MeSeH
R-ATH-351202@@@Metabolism of polyamines
R-ATH-1237112@@@@Methionine salvage pathway
R-ATH-351143@@@@Agmatine biosynthesis
R-ATH-351200@@@@Interconversion of polyamines
R-ATH-70635@@@@Urea cycle
R-ATH-71288@@@@Creatine metabolism
R-ATH-389661@@@Glyoxylate metabolism and glycine degradation
R-ATH-6783984@@@@Glycine degradation
R-ATH-6788656@@@Histidine, lysine, phenylalanine, tyrosine, proline and tryptophan catabolism
R-ATH-70688@@@@Proline catabolism
R-ATH-70921@@@@Histidine catabolism
R-ATH-71064@@@@Lysine catabolism
R-ATH-71182@@@@Phenylalanine and tyrosine catabolism
R-ATH-71240@@@@Tryptophan catabolism
R-ATH-70614@@@Amino acid synthesis and interconversion (transamination)
R-ATH-977347@@@@Serine biosynthesis
R-ATH-70895@@@Branched-chain amino acid catabolism
R-ATH-71262@@@@Carnitine synthesis
R-ATH-71387@@Metabolism of carbohydrates
R-ATH-1630316@@@Glycosaminoglycan metabolism
R-ATH-1638074@@@@Keratan sulfate/keratin metabolism
R-ATH-2022854@@@@@Keratan sulfate biosynthesis
R-ATH-2022857@@@@@Keratan sulfate degradation
R-ATH-1638091@@@@Heparan sulfate/heparin (HS-GAG) metabolism
R-ATH-2022928@@@@@HS-GAG biosynthesis
R-ATH-2024096@@@@@HS-GAG degradation
R-ATH-174362@@@@Transport and synthesis of PAPS
R-ATH-1793185@@@@Chondroitin sulfate/dermatan sulfate metabolism
R-ATH-2024101@@@@@CS/DS degradation
R-ATH-2142845@@@@Hyaluronan metabolism
R-ATH-2160916@@@@@Hyaluronan uptake and degradation
R-ATH-189085@@@Digestion of dietary carbohydrate
R-ATH-189200@@@Hexose transport
R-ATH-70153@@@@Glucose transport
R-ATH-170822@@@@@Regulation of Glucokinase by Glucokinase Regulatory Protein
R-ATH-5652084@@@Fructose metabolism
R-ATH-5652227@@@@Fructose biosynthesis
R-ATH-70350@@@@Fructose catabolism
R-ATH-5653890@@@Lactose synthesis
R-ATH-5661270@@@Catabolism of glucuronate to xylulose-5-phosphate
R-ATH-70326@@@Glucose metabolism
R-ATH-3322077@@@@Glycogen synthesis
R-ATH-70171@@@@Glycolysis
R-ATH-70221@@@@Glycogen breakdown (glycogenolysis)
R-ATH-5357572@@@@@Lysosomal glycogen catabolism
R-ATH-70263@@@@Gluconeogenesis
R-ATH-70370@@@Galactose catabolism
R-ATH-71336@@@Pentose phosphate pathway (hexose monophosphate shunt)
R-ATH-73843@@@5-Phosphoribose 1-diphosphate biosynthesis
R-ATH-8853383@@@Lysosomal oligosaccharide catabolism
R-ATH-71737@@Pyrophosphate hydrolysis
