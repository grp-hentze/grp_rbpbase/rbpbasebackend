R-DME-1430728@Metabolism
R-DME-1362409@@Mitochondrial iron-sulfur cluster biogenesis
R-DME-2395516@@@Electron transport from NADPH to Ferredoxin
R-DME-1428517@@The citric acid (TCA) cycle and respiratory electron transport
R-DME-163200@@@Respiratory electron transport, ATP synthesis by chemiosmotic coupling, and heat production by uncoupling proteins.
R-DME-163210@@@@Formation of ATP by chemiosmotic coupling
R-DME-166187@@@@Mitochondrial Uncoupling Proteins
R-DME-167826@@@@@The fatty acid cycling model
R-DME-167827@@@@@The proton buffering model
R-DME-611105@@@@Respiratory electron transport
R-DME-6799198@@@@@Complex I biogenesis
R-DME-71406@@@Pyruvate metabolism and Citric Acid (TCA) cycle
R-DME-70268@@@@Pyruvate metabolism
R-DME-204174@@@@@Regulation of pyruvate dehydrogenase (PDH) complex
R-DME-71403@@@@Citric acid cycle (TCA cycle)
R-DME-880009@@@@Interconversion of 2-oxoglutarate and 2-hydroxyglutarate
R-DME-1475029@@Reversible hydration of carbon dioxide
R-DME-1480926@@O2/CO2 exchange in erythrocytes
R-DME-1237044@@@Erythrocytes take up carbon dioxide and release oxygen
R-DME-1247673@@@Erythrocytes take up oxygen and release carbon dioxide
R-DME-1483249@@Inositol phosphate metabolism
R-DME-1855167@@@Synthesis of pyrophosphates in the cytosol
R-DME-1855183@@@Synthesis of IP2, IP, and Ins in the cytosol
R-DME-1855191@@@Synthesis of IPs in the nucleus
R-DME-1855204@@@Synthesis of IP3 and IP4 in the cytosol
R-DME-1855231@@@Synthesis of IPs in the ER lumen
R-DME-15869@@Metabolism of nucleotides
R-DME-2393930@@@Phosphate bond hydrolysis by NUDT proteins
R-DME-499943@@@Synthesis and interconversion of nucleotide di- and triphosphates
R-DME-73847@@@Purine metabolism
R-DME-73817@@@@Purine ribonucleoside monophosphate biosynthesis
R-DME-74217@@@@Purine salvage
R-DME-74259@@@@Purine catabolism
R-DME-73848@@@Pyrimidine metabolism
R-DME-500753@@@@Pyrimidine biosynthesis
R-DME-73614@@@@Pyrimidine salvage reactions
R-DME-73621@@@@Pyrimidine catabolism
R-DME-8850843@@@Phosphate bond hydrolysis by NTPDase proteins
R-DME-163685@@Integration of energy metabolism
R-DME-163358@@@PKA-mediated phosphorylation of key metabolic factors
R-DME-163359@@@Glucagon signaling in metabolic regulation
R-DME-164378@@@@PKA activation in glucagon signalling
R-DME-163680@@@AMPK inhibits chREBP transcriptional activation activity
R-DME-163754@@@Insulin effects increased synthesis of Xylulose-5-Phosphate
R-DME-163765@@@ChREBP activates metabolic gene expression
R-DME-422356@@@Regulation of insulin secretion
R-DME-381676@@@@Glucagon-like Peptide-1 (GLP1) regulates insulin secretion
R-DME-399997@@@@Acetylcholine regulates insulin secretion
R-DME-400042@@@@Adrenaline,noradrenaline inhibits insulin secretion
R-DME-400451@@@@Free fatty acids regulate insulin secretion
R-DME-434316@@@@@Fatty Acids bound to GPR40 (FFAR1) regulate insulin secretion
R-DME-189445@@Metabolism of porphyrins
R-DME-189451@@@Heme biosynthesis
R-DME-189483@@@Heme degradation
R-DME-196854@@Metabolism of vitamins and cofactors
R-DME-196849@@@Metabolism of water-soluble vitamins and cofactors
R-DME-196757@@@@Metabolism of folate and pterines
R-DME-196780@@@@Biotin transport and metabolism
R-DME-196807@@@@Nicotinate metabolism
R-DME-197264@@@@@Nicotinamide salvaging
R-DME-196819@@@@Vitamin B1 (thiamin) metabolism
R-DME-196836@@@@Vitamin C (ascorbate) metabolism
R-DME-196843@@@@Vitamin B2 (riboflavin) metabolism
R-DME-199220@@@@Vitamin B5 (pantothenate) metabolism
R-DME-196783@@@@@Coenzyme A biosynthesis
R-DME-947581@@@@Molybdenum cofactor biosynthesis
R-DME-964975@@@@Vitamins B6 activation to pyridoxal phosphate
R-DME-6806667@@@Metabolism of fat-soluble vitamins
R-DME-196791@@@@Vitamin D (calciferol) metabolism
R-DME-6806664@@@@Metabolism of vitamin K
R-DME-975634@@@@Retinoid metabolism and transport
R-DME-202131@@Metabolism of nitric oxide
R-DME-203765@@@eNOS activation and regulation
R-DME-1474151@@@@Tetrahydrobiopterin (BH4) synthesis, recycling, salvage and regulation
R-DME-203615@@@@eNOS activation
R-DME-203641@@@@NOSTRIN mediated eNOS trafficking
R-DME-203754@@@@NOSIP mediated eNOS trafficking
R-DME-211859@@Biological oxidations
R-DME-156580@@@Phase II conjugation
R-DME-156581@@@@Methylation
R-DME-156584@@@@Cytosolic sulfonation of small molecules
R-DME-174362@@@@@Transport and synthesis of PAPS
R-DME-156588@@@@Glucuronidation
R-DME-173599@@@@@Formation of the active cofactor, UDP-glucuronate
R-DME-156590@@@@Glutathione conjugation
R-DME-174403@@@@@Glutathione synthesis and recycling
R-DME-211945@@@Phase 1 - Functionalization of compounds
R-DME-140179@@@@Amine Oxidase reactions
R-DME-141334@@@@@PAOs oxidise polyamines to amines
R-DME-211897@@@@Cytochrome P450 - arranged by substrate type
R-DME-211916@@@@@Vitamins
R-DME-211958@@@@@Miscellaneous substrates
R-DME-211976@@@@@Endogenous sterols
R-DME-211979@@@@@Eicosanoids
R-DME-211981@@@@@Xenobiotics
R-DME-211957@@@@@@Aromatic amines can be N-hydroxylated or N-dealkylated by CYP1A2
R-DME-71384@@@@Ethanol oxidation
R-DME-8937144@@@@Aryl hydrocarbon receptor signalling
R-DME-5423646@@@Aflatoxin activation and detoxification
R-DME-2161522@@Abacavir transport and metabolism
R-DME-2161517@@@Abacavir transmembrane transport
R-DME-2161541@@@Abacavir metabolism
R-DME-556833@@Metabolism of lipids and lipoproteins
R-DME-1483257@@@Phospholipid metabolism
R-DME-1483206@@@@Glycerophospholipid biosynthesis
R-DME-1482788@@@@@Acyl chain remodelling of PC
R-DME-1482798@@@@@Acyl chain remodeling of CL
R-DME-1482801@@@@@Acyl chain remodelling of PS
R-DME-1482839@@@@@Acyl chain remodelling of PE
R-DME-1482883@@@@@Acyl chain remodeling of DAG and TAG
R-DME-1482922@@@@@Acyl chain remodelling of PI
R-DME-1482925@@@@@Acyl chain remodelling of PG
R-DME-1483076@@@@@Synthesis of CL
R-DME-1483101@@@@@Synthesis of PS
R-DME-1483115@@@@@Hydrolysis of LPC
R-DME-1483148@@@@@Synthesis of PG
R-DME-1483166@@@@@Synthesis of PA
R-DME-1483191@@@@@Synthesis of PC
R-DME-1483196@@@@@PI and PC transport between ER and Golgi membranes
R-DME-1483213@@@@@Synthesis of PE
R-DME-1483226@@@@@Synthesis of PI
R-DME-6798163@@@@@Choline catabolism
R-DME-1483255@@@@PI Metabolism
R-DME-1483196@@@@@PI and PC transport between ER and Golgi membranes
R-DME-1483248@@@@@Synthesis of PIPs at the ER membrane
R-DME-1660499@@@@@Synthesis of PIPs at the plasma membrane
R-DME-1660514@@@@@Synthesis of PIPs at the Golgi membrane
R-DME-1660516@@@@@Synthesis of PIPs at the early endosome membrane
R-DME-1660517@@@@@Synthesis of PIPs at the late endosome membrane
R-DME-6814848@@@@@Glycerophospholipid catabolism
R-DME-8847453@@@@@Synthesis of PIPs in the nucleus
R-DME-1655829@@@Regulation of cholesterol biosynthesis by SREBP (SREBF)
R-DME-2426168@@@@Activation of gene expression by SREBF (SREBP)
R-DME-191273@@@Cholesterol biosynthesis
R-DME-6807047@@@@Cholesterol biosynthesis via desmosterol
R-DME-6807062@@@@Cholesterol biosynthesis via lathosterol
R-DME-194068@@@Bile acid and bile salt metabolism
R-DME-159418@@@@Recycling of bile acids and salts
R-DME-192105@@@@Synthesis of bile acids and bile salts
R-DME-193368@@@@@Synthesis of bile acids and bile salts via 7alpha-hydroxycholesterol
R-DME-193775@@@@@Synthesis of bile acids and bile salts via 24-hydroxycholesterol
R-DME-193807@@@@@Synthesis of bile acids and bile salts via 27-hydroxycholesterol
R-DME-196071@@@Metabolism of steroid hormones
R-DME-193048@@@@Androgen biosynthesis
R-DME-193144@@@@Estrogen biosynthesis
R-DME-193993@@@@Mineralocorticoid biosynthesis
R-DME-194002@@@@Glucocorticoid biosynthesis
R-DME-196108@@@@Pregnenolone biosynthesis
R-DME-2046104@@@alpha-linolenic (omega3) and linoleic (omega6) acid metabolism
R-DME-2046105@@@@Linoleic acid (LA) metabolism
R-DME-2046106@@@@alpha-linolenic acid (ALA) metabolism
R-DME-2142753@@@Arachidonic acid metabolism
R-DME-2142670@@@@Synthesis of epoxy (EET) and dihydroxyeicosatrienoic acids (DHET)
R-DME-2142691@@@@Synthesis of Leukotrienes (LT) and Eoxins (EX)
R-DME-2142700@@@@Synthesis of Lipoxins (LX)
R-DME-2142816@@@@Synthesis of (16-20)-hydroxyeicosatetraenoic acids (HETE)
R-DME-2162123@@@@Synthesis of Prostaglandins (PG) and Thromboxanes (TX)
R-DME-2142789@@@Ubiquinol biosynthesis
R-DME-390918@@@Peroxisomal lipid metabolism
R-DME-389542@@@@NADPH regeneration
R-DME-389599@@@@Alpha-oxidation of phytanate
R-DME-389887@@@@Beta-oxidation of pristanoyl-CoA
R-DME-390247@@@@Beta-oxidation of very long chain fatty acids
R-DME-75896@@@@Plasmalogen biosynthesis
R-DME-428157@@@Sphingolipid metabolism
R-DME-1660661@@@@Sphingolipid de novo biosynthesis
R-DME-1660662@@@@Glycosphingolipid metabolism
R-DME-535734@@@Fatty acid, triacylglycerol, and ketone body metabolism
R-DME-200425@@@@Import of palmitoyl-CoA into the mitochondrial matrix
R-DME-74182@@@@Ketone body metabolism
R-DME-77108@@@@@Utilization of Ketone Bodies
R-DME-77111@@@@@Synthesis of Ketone Bodies
R-DME-75109@@@@Triglyceride Biosynthesis
R-DME-75105@@@@@Fatty Acyl-CoA Biosynthesis
R-DME-75876@@@@@@Synthesis of very long-chain fatty acyl-CoAs
R-DME-77289@@@@Mitochondrial Fatty Acid Beta-Oxidation
R-DME-77286@@@@@mitochondrial fatty acid beta-oxidation of saturated fatty acids
R-DME-77285@@@@@@Beta oxidation of myristoyl-CoA to lauroyl-CoA
R-DME-77305@@@@@@Beta oxidation of palmitoyl-CoA to myristoyl-CoA
R-DME-77310@@@@@@Beta oxidation of lauroyl-CoA to decanoyl-CoA-CoA
R-DME-77346@@@@@@Beta oxidation of decanoyl-CoA to octanoyl-CoA-CoA
R-DME-77348@@@@@@Beta oxidation of octanoyl-CoA to hexanoyl-CoA
R-DME-77350@@@@@@Beta oxidation of hexanoyl-CoA to butanoyl-CoA
R-DME-77352@@@@@@Beta oxidation of butanoyl-CoA to acetyl-CoA
R-DME-77288@@@@@mitochondrial fatty acid beta-oxidation of unsaturated fatty acids
R-DME-73923@@@Lipid digestion, mobilization, and transport
R-DME-163560@@@@Hormone-sensitive lipase (HSL)-mediated triacylglycerol hydrolysis
R-DME-174824@@@@Lipoprotein metabolism
R-DME-171052@@@@@LDL-mediated lipid transport
R-DME-174800@@@@@Chylomicron-mediated lipid transport
R-DME-194223@@@@@HDL-mediated lipid transport
R-DME-8855121@@@@@VLDL interactions
R-DME-8866423@@@@@@VLDL biosynthesis
R-DME-192456@@@@Digestion of dietary lipid
R-DME-265473@@@@Trafficking of dietary sterols
R-DME-8848584@@@Wax biosynthesis
R-DME-71291@@Metabolism of amino acids and derivatives
R-DME-1614635@@@Sulfur amino acid metabolism
R-DME-1237112@@@@Methionine salvage pathway
R-DME-1614558@@@@Degradation of cysteine and homocysteine
R-DME-1614517@@@@@Sulfide oxidation to sulfate
R-DME-1614603@@@@Cysteine formation from homocysteine
R-DME-209776@@@Amine-derived hormones
R-DME-209905@@@@Catecholamine biosynthesis
R-DME-209931@@@@Serotonin and melatonin biosynthesis
R-DME-209968@@@@Thyroxine biosynthesis
R-DME-2408522@@@Selenoamino acid metabolism
R-DME-2408508@@@@Metabolism of ingested SeMet, Sec, MeSec into H2Se
R-DME-351202@@@Metabolism of polyamines
R-DME-1237112@@@@Methionine salvage pathway
R-DME-350562@@@@Regulation of ornithine decarboxylase (ODC)
R-DME-351143@@@@Agmatine biosynthesis
R-DME-351200@@@@Interconversion of polyamines
R-DME-70635@@@@Urea cycle
R-DME-71288@@@@Creatine metabolism
R-DME-389661@@@Glyoxylate metabolism and glycine degradation
R-DME-6783984@@@@Glycine degradation
R-DME-6788656@@@Histidine, lysine, phenylalanine, tyrosine, proline and tryptophan catabolism
R-DME-70688@@@@Proline catabolism
R-DME-70921@@@@Histidine catabolism
R-DME-71064@@@@Lysine catabolism
R-DME-71182@@@@Phenylalanine and tyrosine catabolism
R-DME-71240@@@@Tryptophan catabolism
R-DME-70614@@@Amino acid synthesis and interconversion (transamination)
R-DME-977347@@@@Serine biosynthesis
R-DME-70895@@@Branched-chain amino acid catabolism
R-DME-71262@@@@Carnitine synthesis
R-DME-71387@@Metabolism of carbohydrates
R-DME-1630316@@@Glycosaminoglycan metabolism
R-DME-1638074@@@@Keratan sulfate/keratin metabolism
R-DME-2022854@@@@@Keratan sulfate biosynthesis
R-DME-2022857@@@@@Keratan sulfate degradation
R-DME-1638091@@@@Heparan sulfate/heparin (HS-GAG) metabolism
R-DME-1971475@@@@@A tetrasaccharide linker sequence is required for GAG synthesis
R-DME-2022928@@@@@HS-GAG biosynthesis
R-DME-2024096@@@@@HS-GAG degradation
R-DME-174362@@@@Transport and synthesis of PAPS
R-DME-1793185@@@@Chondroitin sulfate/dermatan sulfate metabolism
R-DME-1971475@@@@@A tetrasaccharide linker sequence is required for GAG synthesis
R-DME-2022870@@@@@Chondroitin sulfate biosynthesis
R-DME-2022923@@@@@Dermatan sulfate biosynthesis
R-DME-2024101@@@@@CS/DS degradation
R-DME-2142845@@@@Hyaluronan metabolism
R-DME-2142850@@@@@Hyaluronan biosynthesis and export
R-DME-2160916@@@@@Hyaluronan uptake and degradation
R-DME-189085@@@Digestion of dietary carbohydrate
R-DME-189200@@@Hexose transport
R-DME-70153@@@@Glucose transport
R-DME-170822@@@@@Regulation of Glucokinase by Glucokinase Regulatory Protein
R-DME-5652084@@@Fructose metabolism
R-DME-5652227@@@@Fructose biosynthesis
R-DME-70350@@@@Fructose catabolism
R-DME-5653890@@@Lactose synthesis
R-DME-5661270@@@Catabolism of glucuronate to xylulose-5-phosphate
R-DME-70326@@@Glucose metabolism
R-DME-3322077@@@@Glycogen synthesis
R-DME-70171@@@@Glycolysis
R-DME-70221@@@@Glycogen breakdown (glycogenolysis)
R-DME-70263@@@@Gluconeogenesis
R-DME-70370@@@Galactose catabolism
R-DME-71336@@@Pentose phosphate pathway (hexose monophosphate shunt)
R-DME-73843@@@5-Phosphoribose 1-diphosphate biosynthesis
R-DME-8853383@@@Lysosomal oligosaccharide catabolism
R-DME-71737@@Pyrophosphate hydrolysis
R-DME-8935690@@Miscellaneous digestion events
