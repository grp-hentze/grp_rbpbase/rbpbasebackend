---
title: "human_annotation"
author: "Panagiotis Mantas"
date: "2 May 2017"
output: html_document
---
```{r required libraries echo=FALSE}
require(dplyr)
require("methods")
require("limma")
require("GO.db")
require("biomaRt")
require("Biostrings")
```
General Biomart
```{r}
library(biomaRt)
marts <- listMarts()
head(marts)
datasets <- listDatasets(useMart("ENSEMBL_MART_ENSEMBL"))
head(datasets)
dataset_name <- datasets[grep(pattern="human",x=datasets$description, ignore.case = T),][1]
mart_sapiens <- useMart("ensembl", "hsapiens_gene_ensembl")

ENSG2GO   <-getBM(attributes = c("ensembl_gene_id", "go_id", "name_1006", "namespace_1003"), mart = mart_sapiens)
# Keep the genes that have a go_id
ENSG2GO <- ENSG2GO[nchar(ENSG2GO$go_id) > 0, ]
ENSG2GO <- unique(ENSG2GO)
allgos = unique(ENSG2GO[,c("go_id","name_1006", "namespace_1003")])
# keep an abbreviation for the GO description
allgos[,"namespace_1003"] = gsub("biological_process", "BP", allgos[,"namespace_1003"])
allgos[,"namespace_1003"] = gsub("cellular_component", "CC", allgos[,"namespace_1003"])
allgos[,"namespace_1003"] = gsub("molecular_function", "MF", allgos[,"namespace_1003"])

ENSG2GO <- unique(ENSG2GO[,c("ensembl_gene_id","go_id")])
#Get all the GO terms for each description
gobp = as.list(GOBPANCESTOR)
gomf = as.list(GOMFANCESTOR)
gocc = as.list(GOCCANCESTOR)
go <- c(gobp, gomf, gocc)
n <- sapply(go, length)
DF = data.frame(rep(names(go), n), unlist(go), stringsAsFactors=FALSE)
M <- merge(ENSG2GO, DF, by.x="go_id", by.y="rep.names.go...n.")
M <- unique(M[,2:3])
colnames(M) <- colnames(ENSG2GO)
M <- unique(rbind(ENSG2GO,M))
ENSG2GO <- M
g = GOID(GOTERM)
g.o = Ontology(GOTERM)
g.t = Term(GOTERM)
m = match(ENSG2GO[,"go_id"],g)
ENSG2GO <- cbind(ENSG2GO, name_1006 = g.t[m],
                 namespace_1003 = g.o[m], stringsAsFactors=FALSE)
ENSG2GO <- ENSG2GO[nchar(ENSG2GO[,"go_id"]) != 0,]
#Create a list based on the GO terms (CC, BP, MF)
SP = split(ENSG2GO, ENSG2GO[,"namespace_1003"])
#Create separate df based on the GO terms
ENSG2GOBP <- data.frame(source="GO.BP",
                        SP[["BP"]][,c("ensembl_gene_id", "go_id", "name_1006")],
                        stringsAsFactors=FALSE)
ENSG2GOMF <- data.frame(source="GO.MF",
                        SP[["MF"]][,c("ensembl_gene_id", "go_id", "name_1006")],
                        stringsAsFactors=FALSE)
ENSG2GOCC <- data.frame(source="GO.CC",
                          SP[["CC"]][,c("ensembl_gene_id", "go_id", "name_1006")],
                          stringsAsFactors=FALSE)

#######
# Get the family ID for each gene - 5451 unique entries for genes have a family ID
#######
ENSG2family <- getBM(attributes = c("ensembl_gene_id", 
                                    "family", "family_description"), mart = mart_sapiens)
ENSG2family <- ENSG2family[nchar(ENSG2family[,2]) != 0,]
ENSG2family <- unique(ENSG2family)
ENSG2family <- data.frame(source = "ENSEMBLfamily", ENSG2family,
                          stringsAsFactors=FALSE)



######
# Get the superfamily ID for each gene
######
ENSG2superfamily <- getBM(attributes = c("ensembl_gene_id",
                                        "superfamily"), mart = mart_sapiens)
ENSG2superfamily <- ENSG2superfamily[nchar(ENSG2superfamily[,2]) != 0,]
ENSG2superfamily <- ENSG2superfamily[!is.na(ENSG2superfamily[,2]),]
ENSG2superfamily <- unique(ENSG2superfamily)
ENSG2superfamily <- data.frame(source = "superfamily",
                               ENSG2superfamily, name="", stringsAsFactors=FALSE)




######
# Get the interpro annotation for all genes
######
ENSG2Interpro <- getBM(attributes = c("ensembl_gene_id", "interpro",
                                     "interpro_short_description"), mart = mart_sapiens)
ENSG2Interpro <- ENSG2Interpro[nchar(ENSG2Interpro[,2]) != 0,]
ENSG2Interpro <- unique(ENSG2Interpro)
ENSG2Interpro <- data.frame(source = "interpro", ENSG2Interpro,
                            stringsAsFactors=FALSE)

dplyr::filter(ENSG2Interpro,interpro=="IPR005121")
#####
# Get the chromosome for all genes.
#####
ENSG2chromosome <- getBM(attributes = c("ensembl_gene_id",
                                       "chromosome_name"), mart = mart_sapiens)
ENSG2chromosome <- ENSG2chromosome[nchar(ENSG2chromosome[,2]) != 0,]
ENSG2chromosome <- unique(ENSG2chromosome)
ENSG2chromosome <- data.frame(source = "chromosome",
                             ENSG2chromosome, name="", stringsAsFactors=FALSE)


#####
# Get the Gene names
#####
ENSG2name <- getBM(attributes = c("ensembl_gene_id", "external_gene_name"),mart = mart_sapiens)
ENSG2name <- ENSG2name[nchar(ENSG2name$external_gene_name) > 0,]
ENSG2name <- unique(ENSG2name)

#####
# Prepare Pfam annotation
#####
ENSG2pfam <- getBM(attributes = c("ensembl_gene_id", "pfam"),mart = mart_sapiens)
ENSG2pfam <- ENSG2pfam[nchar(ENSG2pfam$pfam) > 0, ]
ENSG2pfam <- unique(ENSG2pfam)


library(PFAM.db)
AC2DE <- as.list(PFAMDE)
ID2AC <- as.list(PFAMID2AC)
pfamID_AC <- do.call(rbind.data.frame, ID2AC)
pfamID_AC <-cbind(rownames(pfamID_AC),pfamID_AC)
colnames(pfamID_AC) <- c("ID","AC")
pfamDE <- do.call(rbind.data.frame, AC2DE)
colnames(pfamDE) <- "description"

pfamDE <- cbind(rownames(pfamDE),pfamDE)
colnames(pfamDE) <- c("AC","description")
pfam_all <- dplyr::left_join(pfamID_AC,pfamDE, by = c("AC","AC"))


#####
# Get RBD based on pfam annotation
#####
pfam_RBD <- rownames(pfamDE)[grep(ignore.case=F, pattern="RNA recognition motif|RNA binding|RNA-binding",x = pfamDE$description )]
pfam_RBD_description <- pfamDE[grep(ignore.case=F, pattern="RNA recognition motif|RNA binding|RNA-binding",x = pfamDE$description ), ]

RBD_info <- load("./RNA_binding_domains.RData") # loads data.frame called RNA_binding_domains
RNA_binding_domains$pFamName[60] <-"SpoU_sub_bind"
RNA_binding_domains$pFamName[68] <- "tRNA_anti_2"
rna_bindin_domains_edit = data.frame(pFamName="tRNA_anti-codon",Desc="OB-fold nucleic acid binding domain",Type= "Non-Canonical", Notes= "")
RNA_binding_domains <- rbind(RNA_binding_domains,rna_bindin_domains_edit)


table(RNA_binding_domains$Desc %in% as.vector(pfamDE$description))  #FALSE  12 TRUE 78 
# Change the name of the RNA_binding domains according to the pfam v30.0 
domains_not_found <- RNA_binding_domains[which(!RNA_binding_domains$Desc %in% as.vector(pfamDE$description)),]
######
# Change names / simplify names
######
replaceDomains <- function(x) {
   x = sub("RRM_[123456]","RRM",x)
   x = sub("KH_[123]","KH",x)
   x = sub("FAST_[12]","FAST",x)
   x = sub("SAM_[12]","SAM",x)
   x = sub("zf-met","zf-C2H2",x)
   x = sub("zf-C2H2_4","zf-C2H2",x)
   x = sub("zf-C2H2_jaz","zf-C2H2",x)

   x = sub("zf-H2C2_2","zf-H2C2",x)
   x = sub("zf-C2H2_6","zf-C2H2",x)
   x = sub("zf-C3HC4_2","zf-C3HC4",x)
   x = sub("zf-RING_2","zf-RING",x)
   x = sub("Tnp_zf-ribbon_2","Tnp_zf-ribbon",x)
   x = sub("zf-H2C2_5","zf-H2C2",x)
   x = sub("zf-C3HC4_2","zf-C3HC4",x)
   x = sub("zf-RING_3","zf-RING",x)

   x = sub("zf-C3HC4_3","zf-C3HC4",x)
   x = sub("zf-C2H2_2","zf-C2H2",x)
   x = sub("zf-C2H2_3","zf-C2H2",x)
   x = sub("zf-C2HC_2","zf-C2HC",x)
   x = sub("zf-RING_LisH","zf-RING",x)
   x = sub("zf-CCHC_3","zf-CCHC",x)
   x = sub("zf-ribbon_3","zf-ribbon",x)
   x = sub("zf-CCHC_2","zf-CCHC",x)
 x[grepl("Ribosomal",x)] = "Ribosomal"
x
}
pfamID_AC$ID <- sapply(pfamID_AC$ID, replaceDomains)
RNA_binding_domains$pFamName <- sapply(RNA_binding_domains$pFamName, replaceDomains)

domains_not_found <- RNA_binding_domains[which(!RNA_binding_domains$pFamName %in% pfamID_AC$ID),]
library(tibble)
RNA_binding_domains <- as.tibble(RNA_binding_domains)
colnames(RNA_binding_domains) <- c("ID","Desc","Type","Notes")
RNA_binding_domains <- dplyr::left_join(RNA_binding_domains,pfam_all, by=c("ID","ID")) 
Classical_RBD       <- dplyr::filter(.data = RNA_binding_domains, Type == "Classical" )
NonCanonical_RBD    <- dplyr::filter(.data = RNA_binding_domains, Type == "Non-Canonical")
#pfam_all[grep("putative RNA methylase",x =pfam_all$description, ignore.case = T),]

#RNA_binding_domains[49,]$Desc <- "RNA recognition motif. (a.k.a. RRM, RBD, or RNP domain) "


ENSG2category = dplyr::bind_rows(ENSG2GOBP, ENSG2GOMF, ENSG2GOCC, ENSG2pfam,ENSG2Interpro,
                     ENSG2superfamily, ENSG2family,ENSG2chromosome)
#save(Cel2category, file=Cel2categoryFile
#save(Cel2name, file=Cel2nameFile)
```
```{r setup, include=FALSE}
###### 
# Get GO annotation related to RNA
######
getGOChildren <- function(go, GOCHILDREN) {
 xx <- as.list(GOCHILDREN)
 nchange <- length(go)
 while (nchange > 0) {
 goNew = c(go)
 goNew = c(goNew, unlist(xx[go]))
 goNew = unique(goNew)
 goNew <- goNew[!is.na(goNew)]
 nchange <- length(goNew) - length(go)
 go <- goNew
 }
 go
}


BP.RNArelated <- getGOChildren(c("GO:0003723","GO:0050684","GO:0043484","GO:2000232","GO:2000235","GO:0006396","GO:0016070"),GOBPCHILDREN)

CC.RNArelated <- getGOChildren(c("GO:0030529","GO:0035145","GO:0030530","GO:0071204","GO:0031015","GO:0031533","GO:0000176","GO:0005643", "GO:0031380","GO:0030532","GO:0005681","GO:0000347", "GO:0000214","GO:0034245","GO:0005762","GO:0031019",  "GO:0030678","GO:0005761","GO:0005763","GO:0070937","GO:0071075","GO:0000932","GO:0071254","GO:0045293","GO:0043614","GO:0071598","GO:0043186","GO:0005840","GO:0030531","GO:0034719","GO:0010494","GO:0070992","GO:0044207","GO:0070993","GO:0018444","GO:0043527"),GOCCCHILDREN)

MF.RNArelated <- getGOChildren(c("GO:0003723","GO:0004527","GO:0004521",
                                   "GO:0003724"),GOMFCHILDREN)


Interpro.RNArelated <- c("IPR007097","IPR004543","IPR004544","IPR004548","IPR002478",
 "IPR002506","IPR002507","IPR000352","IPR008229","IPR005001",
 "IPR004025","IPR004044","IPR001247","IPR001253","IPR005813",
 "IPR005485","IPR007602","IPR007610","IPR000256","IPR001912",
 "IPR001884","IPR001887","IPR001787","IPR001788","IPR000529",
 "IPR000541","IPR002343","IPR006027","IPR007722","IPR000026",
 "IPR000037","IPR000061","IPR000110","IPR000114","IPR002646",
 "IPR002649","IPR001795","IPR010206","IPR010213","IPR005146",
 "IPR005147","IPR002905","IPR003668","IPR003224","IPR005121",
 "IPR005139","IPR001159","IPR001040","IPR001205","IPR004341",
 "IPR000824","IPR000477","IPR000692","IPR000702","IPR002942",
 "IPR007779","IPR004535","IPR004538","IPR004539","IPR004540",
 "IPR004541","IPR004542","IPR001587","IPR001591","IPR001656",
 "IPR003210","IPR003285","IPR003286","IPR002778","IPR002381",
 "IPR002466","IPR006325","IPR000178","IPR000123","IPR002547",
 "IPR002583","IPR002588","IPR007316","IPR006509","IPR006515",
 "IPR004087","IPR004088","IPR004125","IPR000748","IPR000752",
 "IPR002004","IPR002143","IPR002148","IPR002166","IPR001406",
 "IPR007783","IPR001550","IPR001662","IPR001561","IPR001566",
 "IPR001568","IPR008111","IPR000100","IPR002693","IPR002702",
 "IPR002730","IPR002735","IPR005872","IPR005874","IPR005878",
 "IPR005879","IPR005880","IPR007010","IPR003720","IPR003751",
 "IPR001900","IPR006289","IPR004780","IPR004458","IPR004476",
 "IPR001059","IPR001009","IPR001014","IPR001021","IPR001313",
 "IPR001326","IPR001352","IPR005662","IPR006847","IPR004368",
 "IPR004373","IPR004374","IPR004403","IPR000605","IPR002344",
 "IPR002319","IPR004613","IPR004665","IPR001816","IPR001850",
 "IPR001890","IPR001892","IPR001950","IPR001537","IPR000607",
 "IPR002873","IPR006529","IPR006536","IPR006546","IPR006548",
 "IPR001288","IPR000999","IPR001016","IPR003029","IPR015047",
 "IPR015799","IPR012541","IPR009105","IPR010304","IPR008905",
 "IPR008395","IPR016020","IPR013150","IPR014780","IPR011804",
 "IPR011805","IPR009145","IPR011068","IPR011768","IPR012006",
 "IPR019906","IPR019907","IPR019977","IPR019980","IPR012959",
 "IPR012915","IPR009469","IPR015190","IPR015191","IPR015225",
 "IPR015423","IPR016189","IPR016190","IPR016191","IPR009374",
 "IPR015971","IPR008723","IPR008732","IPR008744","IPR018079",
 "IPR018101","IPR018104","IPR013699","IPR015498","IPR015847",
 "IPR015848","IPR016075","IPR018496","IPR019770","IPR014039",
 "IPR011907","IPR014944","IPR012562","IPR009179","IPR015240",
 "IPR008832","IPR008858","IPR009018","IPR005231","IPR006145",
 "IPR006196","IPR006224","IPR006225","IPR013795","IPR011113",
 "IPR011023","IPR011488","IPR020119","IPR014720","IPR009019",
 "IPR009522","IPR014038","IPR014789","IPR014822","IPR011760",
 "IPR008705","IPR015820","IPR006116","IPR006117","IPR007504",
 "IPR012162","IPR011400","IPR009967","IPR018314","IPR022802",
 "IPR020934","IPR021154","IPR019970","IPR018995","IPR018188",
 "IPR020055","IPR020094","IPR020095","IPR016848","IPR020040",
 "IPR020189","IPR022481","IPR017705","IPR020526","IPR018258",
 "IPR018268","IPR016304","IPR017091","IPR020321","IPR020606",
 "IPR017924","IPR019769","IPR018111","IPR013810","IPR018269",
 "IPR018280","IPR020097","IPR020103","IPR019813","IPR019814",
 "IPR019815","IPR013188","IPR016400","IPR020536","IPR020883",
 "IPR020853","IPR020814","IPR020815","IPR020813","IPR020918",
 "IPR000504","IPR006630","IPR018835","IPR015464","IPR008111",
 "IPR002344","IPR018222","IPR015016","IPR016686","IPR015463",
 "IPR009145","IPR015465","IPR015466","IPR012677","IPR009022",
 "IPR015903","IPR019582","IPR003954","IPR005637","IPR005121",
 "IPR002483","IPR015462")

```
```{r}
#######
# List of available gene identifiers
#######
ENSG = sort(unique(c(ENSG2name$ensembl_gene_id,
                       ENSG2Interpro$ensembl_gene_id,
                       ENSG2GOBP$ensembl_gene_id,
                       ENSG2GOMF$ensembl_gene_id,
                       ENSG2GOCC$ensembl_gene_id,
                       ENSG2pfam$ensembl_gene_id) ))

length(ENSG) #  63898
```
```{r}
#####
# Create a dataset with the RNAbinding RNArelated information for each gene
#####
ENSGannotation = data.frame(name = rep("",length(ENSG)), 
                           ENSEMBL = "",
                           RNAbinding = FALSE,
                           RNArelated = FALSE,
                           uncharacterized = FALSE,
                           RBD=FALSE,
                           RNArelatedInterpro = FALSE,
                           RNArelatedGOBP = FALSE,
                           RNArelatedGOMF = FALSE,
                           RNArelatedGOCC = FALSE,
                           stringsAsFactors=FALSE)
row.names(ENSGannotation) = ENSG

ENSGannotation$name     <- tapply(ENSG2name$external_gene_name,
                                 factor(ENSG2name$ensembl_gene_id, levels=ENSG),
                                 function(x) { paste(x, collapse=";") } )
ENSGannotation$ENSEMBL  <- tapply(ENSG2name$ensembl_gene_id,
                                  factor(ENSG2name$ensembl_gene_id, levels=ENSG),
                                  function(x) { paste(x, collapse=";") } )
ENSGannotation$RNAbinding = tapply(ENSG2GOMF$go_id %in% MF.RNArelated,
                                   factor(ENSG2GOMF$ensembl_gene_id, levels=ENSG),
                                   function(x) { any(x) } )
ENSGannotation$RNAbinding[is.na(ENSGannotation$RNAbinding)] = FALSE

ENSGannotation$RNArelatedGOMF <-tapply(ENSG2GOMF$go_id %in% MF.RNArelated,
                                   factor(ENSG2GOMF$ensembl_gene_id, levels=ENSG),
                                   function(x) { any(x) } )
ENSGannotation$RNArelatedGOMF[is.na(ENSGannotation$RNArelatedGOMF)] = FALSE

ENSGannotation$RNArelatedGOCC <- tapply(ENSG2GOCC$go_id %in% CC.RNArelated,
                                        factor(ENSG2GOCC$ensembl_gene_id, levels=ENSG),
                                        function(x) { any(x) })
ENSGannotation$RNArelatedGOCC[is.na(ENSGannotation$RNArelatedGOCC)] = FALSE

ENSGannotation$RNArelatedGOBP <- tapply(ENSG2GOCC$go_id %in% BP.RNArelated,
                                        factor(ENSG2GOCC$ensembl_gene_id, levels=ENSG),
                                        function(x) { any(x) })
ENSGannotation$RNArelatedGOBP[is.na(ENSGannotation$RNArelatedGOBP)] = FALSE


ENSGannotation$RNArelatedInterpro <- tapply(ENSG2Interpro$interpro %in% Interpro.RNArelated,
                                            factor(ENSG2Interpro$ensembl_gene_id, levels=ENSG),
                                            function(x) { any(x) })
ENSGannotation$RNArelatedInterpro[is.na(ENSGannotation$RNArelatedInterpro)] = FALSE


ENSGannotation[which(ENSGannotation$ENSEMBL %in% ENSG2pfam[which(ENSG2pfam$pfam %in% as.vector(Classical_RBD$AC)),1] ),"RBD"] = "Canonical"
ENSGannotation[which(ENSGannotation$ENSEMBL %in% ENSG2pfam[which(ENSG2pfam$pfam %in% as.vector(NonCanonical_RBD$AC)),1] ),"RBD"] = "NonCanonical"
ENSGannotation$RBD[ENSGannotation$RBD == FALSE] = "Unknown"


ENSGannotation$RNArelated <-  ENSGannotation$RNArelatedGOMF | ENSGannotation$RNArelatedGOCC|ENSGannotation$RNArelatedGOBP |ENSGannotation$RNArelatedInterpro 
ENSGannotation$RNArelated[is.na(ENSGannotation$RNArelated)] = FALSE



ENSGannotation$uncharacterized = !(ENSGannotation$ENSEMBL %in% ENSG2GO[[1]]) & + !(ENSGannotation$ENSEMBL %in% ENSG2Interpro[[1]])

save(ENSGannotation,file = "./ENSGannotationFile.rda")
```