# conda create -n opentargets python=2.7 pip pandas
# 
# # Windows:
# conda activate opentargets
# # source activate opentargets
#
# pip install opentargets==3.1.14

# python2.7

import os
from opentargets import OpenTargetsClient
outfile = os.path.join("all_direct.tsv")

if not isfile(outfile):
    client = OpenTargetsClient()
    response = client.filter_associations()
    response.filter(direct=True)
    pd = response.to_dataframe()
    pd.to_csv(outfile, encoding = "utf-8", sep="\t")

